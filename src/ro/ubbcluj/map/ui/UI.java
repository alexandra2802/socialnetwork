package ro.ubbcluj.map.ui;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.Message;
import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.model.validators.RepoException;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.service.FriendshipService;
import ro.ubbcluj.map.service.MessageService;
import ro.ubbcluj.map.service.Network;
import ro.ubbcluj.map.service.UserService;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

public class UI {
    UserService userService;
    FriendshipService friendshipService;
    MessageService messageService;
    Network network;

    public UI(UserService userService,
              FriendshipService friendshipService,
              MessageService messageService,
              Network network) {
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.messageService=messageService;
        this.network = network;
    }

    public void display_menu()
    {
        System.out.println("---------------------------MENU---------------------------");
        System.out.println("Users CRUD:");
        System.out.println("    1.Add user");
        System.out.println("    2.Delete user by id");
        System.out.println("    3.Update user by id");
        System.out.println("Friendships CRUD:");
        System.out.println("    4.Add friendship");
        System.out.println("    5.Delete friendship by id");
        System.out.println("    6.Update friendship by id");
        System.out.println("u.Show all users");
        System.out.println("f.Show all friendships");
        System.out.println("Communities info:");
        System.out.println("    7.Show number of communities");
        System.out.println("    8.Show communities");
        System.out.println("    9.Show most sociable community");
        System.out.println("Friend request:");
        System.out.println("    10.Send friend request");
        System.out.println("    11.Approve friend request");
        System.out.println("    12.Reject friend request");
        System.out.println("-----------------------------------------------------------");
        System.out.println("    13.Show all friends of an user");
        System.out.println("    14.Show all friends of an user created in a specific month of a year");

        System.out.println("s.Send message");
        System.out.println("r.Reply to message");
        System.out.println("c.Show conversation");
        System.out.println("x.Exit");
        System.out.println("-----------------------------------------------------------");
        System.out.println("Choose an option: ");
    }

    public void run_menu()
    {
        display_menu();
        Scanner scanner = new Scanner(System.in);
        String op=scanner.nextLine();
        while(!op.equals("x"))
        {
            if(op.equals("1"))
                addUser();
            if(op.equals("2"))
                deleteUSer();
            if(op.equals("3"))
                updateUser();
            if(op.equals("u"))
                showAllUsers();
            if(op.equals("4"))
                addFriendship();
            if(op.equals("5"))
                deleteFriendship();
            if(op.equals("6"))
                updateFriendship();
            if(op.equals("f"))
                showAllFrienships();
            if(op.equals("7"))
                numberOfCommunities();
            if(op.equals("8"))
                showCommunities();
            if(op.equals("9"))
                mostSociableCommunity();
            if(op.equals("10"))
                sendFriendRequest();
            if(op.equals("11"))
                approveFriendRequest();
            if(op.equals("12"))
                rejectFreindRequest();
            if(op.equals("13"))
                showFriends();
            if(op.equals("14"))
                showFriendsByMonth();
            if(op.equals("s"))
                sendMessage();
            if(op.equals("r"))
                replyToMessage();
            if(op.equals("c"))
                showConversation();
            display_menu();
            op=scanner.nextLine();
        }

    }

    private void showFriendsByMonth() {
        Scanner long_scanner = new Scanner(System.in);
        System.out.println("Id user: ");
        long id = 0;
        id = long_scanner.nextLong();

        System.out.println("Month: ");
        int month= long_scanner.nextInt();

        friendshipService.getFriendsByMonth(id,month);
    }

    private void showFriends() {
        Scanner long_scanner = new Scanner(System.in);
        System.out.println("Id user: ");
        long id = 0;
        id = long_scanner.nextLong();

        friendshipService.getFriends(id);
    }

    public void showAllUsers()
    {
        for(User u:userService.findAll())
            System.out.println(u);
    }

    public void showAllFrienships()
    {
        for(Friendship f:friendshipService.findAll())
            System.out.println(f);
    }

    public void addUser()
    {
        try
        {
            Scanner string_scanner = new Scanner(System.in);
            System.out.println("First name: ");
            String firstName= string_scanner.nextLine();
            System.out.println("Last name: ");
            String lastName=string_scanner.nextLine();
            this.userService.add(firstName,lastName);
            System.out.println("User has been added");
        }
        catch (ValidationException e1)
        {
            System.out.println(e1.getMessage());
        }
        catch (IllegalArgumentException e2)
        {
            System.out.println(e2.getMessage());
        }
        catch(IOException e3)
        {
            System.out.println(e3.getMessage());
        }
        catch (RepoException e4)
        {
            System.out.println(e4.getMessage());
        }catch (InputMismatchException e)
        {
            System.out.println("Incorrect type for id");
        }

    }

    public void deleteUSer ()
    {
        try
        {
            System.out.println("id: ");
            Scanner scanner = new Scanner(System.in);
            long id=scanner.nextLong();
            userService.delete(id);
            System.out.println("User has been deleted");
        }
        catch (IllegalArgumentException e1)
        {
            System.out.println(e1.getMessage());
        }
        catch (RepoException e2)
        {
            System.out.println(e2.getMessage());
        }
        catch(IOException e3)
        {
            System.out.println(e3.getMessage());
        }catch (InputMismatchException e)
        {
            System.out.println("Incorrect type for id");
        }catch (NullPointerException e)
        {
            System.out.println("Entity was not found");
        }

    }

    public void updateUser()  {

        try
        {
            Scanner scanner = new Scanner(System.in);
            System.out.println("id: ");
            long id=scanner.nextLong();
            Scanner scanner2 = new Scanner(System.in);
            System.out.println("New first name(press enter to not change): ");
            String firstName= scanner2.nextLine();
            System.out.println("New last name(press enter to not change): ");
            String lastName=scanner2.nextLine();
            userService.update(id,firstName,lastName);
            System.out.println("User has been updated");
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
        catch (RepoException e)
        {
            System.out.println(e.getMessage());
        }
        catch (IllegalArgumentException e)
        {
            System.out.println(e.getMessage());
        }catch (InputMismatchException e)
        {
            System.out.println("Incorrect type for id");
        }catch (NullPointerException e)
        {
            System.out.println("Entity was not found");
        }
    }

    public void addFriendship()
    {
        try
        {
            Scanner scanner=new Scanner(System.in);
            System.out.println("ID user1: ");
            long idUser1= scanner.nextLong();
            System.out.println("ID user2: ");
            long idUser2= scanner.nextLong();
            //System.out.println("ID friendship: ");
            //long idFriendship=scanner.nextLong();

            //data se genereaza automat cu data curenta
            Date date = new Date();
            SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
            String stringDate= DateFor.format(date);

            friendshipService.add(idUser1,idUser2,stringDate);
            System.out.println("Friendship has been added");
        }catch (InputMismatchException e)
        {
            System.out.println("Incorrect type for id");
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }catch (RepoException e)
        {
            System.out.println(e.getMessage());
        }

    }

    public void deleteFriendship()
    {
        try
        {
            Scanner scanner=new Scanner(System.in);
            System.out.println("id: ");
            long id=scanner.nextLong();
            friendshipService.delete(id);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        catch (InputMismatchException e)
        {
            System.out.println("Incorrect type for id");
        }
        catch(RepoException e)
        {
            System.out.println(e.getMessage());
        }catch (NullPointerException e)
        {
            System.out.println("Entity was not found");
        }
    }

    public void updateFriendship()
    {
        try
        {
            Scanner scanner=new Scanner(System.in);
            Scanner stringScanner=new Scanner(System.in);
            System.out.println("id: ");
            long id=scanner.nextLong();
            System.out.println("New user1 id(press enter to not change): ");
            String id1=stringScanner.nextLine();
            System.out.println("New user2 id(press enter to not change): ");
            String id2=stringScanner.nextLine();
            System.out.println("Date (dd/mm/yyyy): ");
            String stringDate=stringScanner.nextLine();
            friendshipService.update(id,id1,id2,stringDate);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        catch (InputMismatchException e)
        {
            System.out.println("Incorrect type for id");
        }
        catch(RepoException e)
        {
            System.out.println(e.getMessage());
        }
        catch (NumberFormatException e)
        {
            System.out.println("Incorrect type for user id");
        }catch (NullPointerException e)
        {
            System.out.println("Entity was not found");
        }
    }

    public void numberOfCommunities()
    {
        System.out.println(network.numberOfCommunities());
    }

    public void showCommunities()
    {
        for(HashSet<Long> community: network.communities())
        {
            System.out.println(community);
        }
    }

    private void mostSociableCommunity()
    {
        System.out.println(network.mostSociableCommunity());
    }


    public void sendFriendRequest()
    {
        Scanner longScanner=new Scanner(System.in);
        System.out.println("From user id: ");
        long from_id=longScanner.nextLong();
        System.out.println("To user id: ");
        long to_id=longScanner.nextLong();
        User from=userService.findOne(from_id);
        User to=userService.findOne(to_id);
        try
        {
            userService.sendFriendRequest(from,to);

        }catch (RepoException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void approveFriendRequest()
    {
        Scanner longScanner=new Scanner(System.in);
        System.out.println("From user id: ");
        long from_id=longScanner.nextLong();
        System.out.println("To user id: ");
        long to_id=longScanner.nextLong();
        User from=userService.findOne(from_id);
        User to=userService.findOne(to_id);
        userService.approveFriendRequest(from,to);
        //data se genereaza automat cu data curenta
        Date date = new Date();
        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
        String stringDate= DateFor.format(date);

        try
        {
            friendshipService.add(from_id,to_id,stringDate);
        }catch (RepoException e)
        {
            System.out.println(e.getMessage());
        }catch (IOException e)
        {
            System.out.println(e.getMessage());
        }

    }

    public void rejectFreindRequest()
    {
        Scanner longScanner=new Scanner(System.in);
        System.out.println("From user id: ");
        long from_id=longScanner.nextLong();
        System.out.println("To user id: ");
        long to_id=longScanner.nextLong();
        User from=userService.findOne(from_id);
        User to=userService.findOne(to_id);
        userService.rejectFriendRequest(from,to);

    }

    public void sendMessage()
    {
        Scanner longScanner=new Scanner(System.in);
        System.out.println("From(user id): ");
        long from_id=longScanner.nextLong();
        User from=userService.findOne(from_id);
        List<User> to_list=new ArrayList<User>();
        long to_id = 0;
        System.out.println("To(comma separated list of user ids):");
        Scanner s=new Scanner(System.in);
        String to_users=s.nextLine();
        String[] parsedUsers=to_users.split(",");
        for(int i=0;i<parsedUsers.length;i++)
        {
            User to=userService.findOne(Integer.parseInt(parsedUsers[i]));
            to_list.add(to);
        }
        System.out.println("Text: ");
        Scanner stringScanner=new Scanner(System.in);
        String text=stringScanner.nextLine();
        messageService.save(text, LocalDateTime.now(),from,to_list,null);
    }

    public void replyToMessage()
    {
        Scanner longScanner =new Scanner(System.in);
        System.out.println("Enter your id: ");
        Long id1=longScanner.nextLong();
        System.out.println("User to reply to: ");
        Long id2=longScanner.nextLong();
        List<Message> conversation=messageService.findConversation(id1,id2);
        Message lastInConversation=conversation.get(conversation.toArray().length-1);
        System.out.println("Text: ");
        Scanner stringScanner =new Scanner(System.in);
        String text=stringScanner.nextLine();
        User to=userService.findOne(id2);
        List<User> toList=new ArrayList<User>();
        toList.add(to);
        User from =userService.findOne(id1);
        messageService.save(text,LocalDateTime.now(),from,toList,lastInConversation);
    }

    private void showConversation() {
        Scanner s=new Scanner(System.in);
        System.out.println("Show conversation between user: ");
        long id1=s.nextLong();
        System.out.println("and user: ");
        long id2=s.nextLong();
        messageService.findConversation(id1,id2).forEach(System.out::println);
    }




}
