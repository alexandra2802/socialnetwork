package ro.ubbcluj.map.model;

public enum FriendRequestStatus {
    PENDING,APPROVED,REJECTED;
}
