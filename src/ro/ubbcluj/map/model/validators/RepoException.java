package ro.ubbcluj.map.model.validators;

public class RepoException extends RuntimeException{
    public RepoException(String message) {
        super(message);
    }
}
