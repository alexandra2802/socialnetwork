package ro.ubbcluj.map;

import ro.ubbcluj.map.model.Message;
import ro.ubbcluj.map.model.User;

import ro.ubbcluj.map.model.validators.*;

import ro.ubbcluj.map.repository.FriendshipFileRepository;
import ro.ubbcluj.map.repository.UserFileRepository;
import ro.ubbcluj.map.repository.db.FriendRequestDbRepository;
import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
import ro.ubbcluj.map.repository.db.MessageDbRepository;
import ro.ubbcluj.map.repository.db.UserDbRepository;
import ro.ubbcluj.map.service.FriendshipService;
import ro.ubbcluj.map.service.MessageService;
import ro.ubbcluj.map.service.Network;
import ro.ubbcluj.map.service.UserService;
import ro.ubbcluj.map.ui.UI;


public class Main {

    public static void main(String[] args) {
//        TestsInMemoryRepo t=new TestsInMemoryRepo();
//        t.test();
//        TestsFileRepo tFile=new TestsFileRepo();
//        tFile.test();
//        TestsDbRepo tDb=new TestsDbRepo();
//        tDb.test();

        UserValidator userValidator=new UserValidator();
        UserDbRepository userRepo=new UserDbRepository("jdbc:postgresql://localhost:5432/SocialNetwork",
                                                        "postgres",
                                                       "ruxandra.007",
                                                        userValidator);

        FriendRequestValidator friendRequestValidator=new FriendRequestValidator();
        FriendRequestDbRepository friendRequestRepo=new FriendRequestDbRepository("jdbc:postgresql://localhost:5432/SocialNetwork",
                "postgres",
                "ruxandra.007",
                friendRequestValidator,
                userRepo);

        FriendshipValidator friendshipValidator=new FriendshipValidator();
        FriendshipDbRepository friendshipRepo = new FriendshipDbRepository(
                                                        "jdbc:postgresql://localhost:5432/SocialNetwork",
                                                        "postgres",
                                                        "ruxandra.007",
                                                         friendshipValidator,
                                                        friendRequestRepo
                                                        );

        UserService userService=new UserService(userRepo,friendRequestRepo);
        FriendshipService friendshipService=new FriendshipService(friendshipRepo,userRepo);

        MessageValidator messageValidator=new MessageValidator();
        MessageDbRepository messageDbRepository=new MessageDbRepository("jdbc:postgresql://localhost:5432/SocialNetwork",
                                                                "postgres",
                                                                "ruxandra.007",
                                                                messageValidator,
                                                                userRepo);
        MessageService messageService=new MessageService(messageDbRepository);

        Network network=new Network(userService,friendshipService);
        UI ui=new UI(userService,friendshipService,messageService,network);
        ui.run_menu();
    }
}
