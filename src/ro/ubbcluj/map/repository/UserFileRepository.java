package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.model.validators.Validator;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class UserFileRepository extends AbstractFileRepository<Long, User> {
    //private String filename;

    public UserFileRepository(Validator<User> validator, String filename) {
        super(filename,validator);
        this.fileName = filename;
        super.loadData();
    }

    @Override
    public User extractEntity(List<String> attributes) {
        //attributes[0]=1(id)  attributes[1]=Ion(first_name)  attributes[2]=Pop(last_name)
        User user= new User(attributes.get(1),attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }

    @Override
    protected String createEntityAsString(User u) {
        return u.getId() + ";" + u.getFirst_name() + ";" + u.getLast_name();
    }
}