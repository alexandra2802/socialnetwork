package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.model.validators.Validator;

import java.util.List;

public class FriendshipFileRepository extends AbstractFileRepository<Long, Friendship>{
    UserFileRepository userRepo;

    public FriendshipFileRepository(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
        this.fileName = fileName;
        super.loadData();
    }

    @Override
    public Friendship extractEntity(List<String> attributes) {

        Friendship f= new Friendship(Long.parseLong(attributes.get(1)),Long.parseLong(attributes.get(2)),attributes.get(3));
        f.setId(Long.parseLong(attributes.get(0)));
        return f;
    }

    @Override
    protected String createEntityAsString(Friendship f) {
        //10;1;2
        return f.getId()+";"+f.getuser1ID()+";"+f.getuser2ID()+";"+f.getDate();
    }
}
