package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.model.validators.RepoException;
import ro.ubbcluj.map.model.validators.Validator;


import java.io.*;
import java.util.Arrays;
import java.util.List;


///Aceasta clasa implementeaza sablonul de proiectare Template Method; puteti inlucui solutia propusa cu un Factori (vezi mai jos)

public abstract class AbstractFileRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E> {
    String fileName;

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
    }

    public void setFilename(String filename)
    {
        this.fileName=filename;
    }

    protected void loadData() {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String linie;
            while ((linie = br.readLine()) != null) {
                List<String> attr = Arrays.asList(linie.split(";"));
                E e = extractEntity(attr);
                super.save(e);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *  extract entity  - template method design pattern
     *  creates an entity of type E having a specified list of @code attributes
     * @param attributes
     * @return an entity of type E
     */
    public abstract E extractEntity(List<String> attributes);
    ///Observatie-Sugestie: in locul metodei template extractEntity, puteti avea un factory pr crearea instantelor entity

    protected abstract String createEntityAsString(E entity);

    @Override
    public E save(E entity){
        return super.save(entity);
    }

    protected void saveToFile() throws IOException {
        FileWriter f=new FileWriter(fileName, false);
        f.close();

        for(E e:this.findAll())
        {
            try (BufferedWriter bW = new BufferedWriter(new FileWriter(fileName, true))) {
                bW.write(createEntityAsString(e));
                bW.newLine();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    public void addEntity(E entity) throws IOException {
        save(entity);
        saveToFile();
    }

    public void deleteEntity(E entity) throws IOException,RepoException {
        this.remove(entity);
        saveToFile();
    }

    public void updateEntity(E entity,E newEntity) throws IOException {
        this.update(entity,newEntity);
        saveToFile();
    }

}

