package ro.ubbcluj.map.repository;

import ro.ubbcluj.map.model.Entity;
import ro.ubbcluj.map.model.validators.RepoException;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.model.validators.Validator;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E>{
    private Map<ID, E> entities;
    private Validator<E> validator;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        this.entities = new HashMap<>();
    }

    @Override
    public E findOne(ID id) {
        if(id==null)
            throw new IllegalArgumentException("Id must not be null");
        for(Entity<ID> e: entities.values())
            if(e.getId()==id)
                return entities.get(id);
        throw new RepoException("Entity was not found");
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public E save(E entity) {
        if(entity==null)
            throw new RepoException("entity must not be null");
        validator.validate(entity);
        if (entities.get(entity.getId())!=null)
        {
            throw new RepoException("Id already exists");
           // return entity;
        }
        entities.put(entity.getId(), entity);
        return null;
    }

    @Override
    public void remove(E entity) throws RepoException{
        if(entity!=null)
        {
            if(this.findOne(entity.getId())==null)
                throw new RepoException("Entity not found");
            entities.remove(entity.getId());
        }
        else throw new RepoException("Entity not found");

    }

    public void update(E entity,E newEntity)
    {
        if(findOne(entity.getId())==null)
            throw new RepoException("Entity not found");
        entities.remove(entity.getId());
        validator.validate(newEntity);
        entities.put(newEntity.getId(),newEntity);
    }

}
