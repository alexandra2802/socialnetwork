package ro.ubbcluj.map.service;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.model.validators.RepoException;
import ro.ubbcluj.map.repository.FriendshipFileRepository;
import ro.ubbcluj.map.repository.UserFileRepository;
import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
import ro.ubbcluj.map.repository.db.UserDbRepository;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FriendshipService {
    FriendshipDbRepository repo;
    UserDbRepository userRepo;

    public FriendshipService(FriendshipDbRepository repo,UserDbRepository userRepo) {
        this.repo = repo;
        this.userRepo=userRepo;
    }

    public void add( long id1,long id2,String date) throws IOException,RepoException {
        if(userRepo.findOne(id1)==null)
            throw new RepoException("User1 was not found");
        if(userRepo.findOne(id2)==null)
            throw new RepoException("User2 was not found");
        Friendship f=new Friendship(id1,id2,date);
       repo.save(f);
    }

    public void delete(Long id) throws IOException {
        Friendship f=repo.findOne(id);
        repo.remove(f);
    }

    public void update(Long id_friendship,String newId1,String newId2, String newdate) throws IOException {
        Friendship oldFriendship=repo.findOne(id_friendship);

        long newLongId1;
        if(Objects.equals(newId1, ""))
            newLongId1=oldFriendship.getuser1ID();
        else
            newLongId1=Long.parseLong(newId1);
        long newLongId2;
        if(Objects.equals(newId2, ""))
            newLongId2=oldFriendship.getuser2ID();
        else
            newLongId2=Long.parseLong(newId2);
        if(Objects.equals(newdate,""))
            newdate=oldFriendship.getDate();
        if(userRepo.findOne(newLongId1)==null)
            throw new RepoException("User1 was not found");
        if(userRepo.findOne(newLongId2)==null)
            throw new RepoException("User2 was not found");
        Friendship newFreindship=new Friendship(newLongId1,newLongId2,newdate);
        newFreindship.setId(id_friendship);
        repo.update(oldFriendship,newFreindship);
    }

    public Iterable<Friendship> findAll()
    {
        return repo.findAll();
    }


    public void getFriends(long id){
        List<Friendship> listaFriendship = new ArrayList<Friendship>();
        repo.findAll().forEach(listaFriendship::add);
        listaFriendship.stream()
                .filter(f -> f.getuser1ID() == id || f.getuser2ID() == id)
                .forEach(f->{
                    if(f.getuser1ID()!=id) {
                        User user = userRepo.findOne(f.getuser1ID());
                        System.out.println(user.getFirst_name()+ " "+user.getLast_name() + " " + f.getDate());}
                    if(f.getuser2ID()!=id) {
                        User user = userRepo.findOne(f.getuser2ID());
                        System.out.println(user.getFirst_name()+ " "+user.getLast_name()+ " " + f.getDate());}
                });
    }

    public void getFriendsByMonth(long id, int month) {
        List<Friendship> listaFriendship = new ArrayList<Friendship>();
        repo.findAll().forEach(listaFriendship::add);
        listaFriendship.stream()
                .filter(f -> (f.getuser1ID() == id || f.getuser2ID() == id) && (month == Integer.parseInt(String.valueOf(f.getDate().charAt(3))+String.valueOf(f.getDate().charAt(4)))))
                .forEach(f->{
                    if(f.getuser1ID()!=id) {
                        User user = userRepo.findOne(f.getuser1ID());
                        System.out.println(user.getFirst_name()+ " "+user.getLast_name()+ " " + f.getDate());}
                    if(f.getuser2ID()!=id) {
                        User user = userRepo.findOne(f.getuser2ID());
                        System.out.println(user.getFirst_name()+ " "+user.getLast_name()+ " " + f.getDate());}
                });
    }
}
