package ro.ubbcluj.map.service;

import ro.ubbcluj.map.model.Message;
import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.repository.db.MessageDbRepository;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageService {
    MessageDbRepository messageDbRepo;

    public MessageService(MessageDbRepository messageDbRepo) {
        this.messageDbRepo = messageDbRepo;
    }

    public Message findOne(Long aLong)
    {
        return messageDbRepo.findOne(aLong);
    }

    public Iterable<Message> findAll()
    {
        return messageDbRepo.findAll();
    }

    public int getSize()
    {
        return messageDbRepo.getSize();
    }

    public void save(String text,
                     LocalDateTime dateTime,
                     User from,
                     List<User> to,
                     Message replying_to)
    {
        Message m=new Message(from,to,text,dateTime,replying_to);
        this.messageDbRepo.save(m);
    }

    public List<Message> findConversation(Long u1_id, Long u2_id)
    {
        return messageDbRepo.findConversation(u1_id,u2_id);
    }
}
