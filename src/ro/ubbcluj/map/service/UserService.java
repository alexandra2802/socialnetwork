package ro.ubbcluj.map.service;

import ro.ubbcluj.map.model.FriendRequest;
import ro.ubbcluj.map.model.FriendRequestStatus;
import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.model.validators.RepoException;
import ro.ubbcluj.map.repository.UserFileRepository;
import ro.ubbcluj.map.repository.db.FriendRequestDbRepository;
import ro.ubbcluj.map.repository.db.UserDbRepository;

import java.io.IOException;

public class UserService {
    private UserDbRepository repo;
    FriendRequestDbRepository friendRequestDbRepo;

    public UserService(UserDbRepository repo,FriendRequestDbRepository friendRequestDbRepo) {
        this.repo = repo;
        this.friendRequestDbRepo=friendRequestDbRepo;
    }

    public void add(String firstName,String lastName) throws IOException {
        User u=new User(firstName,lastName);
        repo.save(u);

    }

    public void delete(long id) throws IOException, RepoException {
        User u=repo.findOne(id);
        repo.remove(u);
    }

    public void update(Long id,String newFirstName,String newLastName) throws IOException {
        User oldUser=repo.findOne(id);

            if(newFirstName.equals(""))
                newFirstName= oldUser.getFirst_name();
            if(newLastName.equals(""))
                newLastName= oldUser.getLast_name();
            User newUser=new User(newFirstName,newLastName);
            newUser.setId(id);
            repo.update(oldUser,newUser);

    }

    public  Iterable<User> findAll()
    {
        return repo.findAll();
    }

    public int  getSize()
    {
        return repo.getSize();
    }

    public User findOne(long id)
    {
        return repo.findOne(id);
    }

    public void sendFriendRequest(User from,User to)
    {
        FriendRequest fr=new FriendRequest(from,to, FriendRequestStatus.PENDING);
        friendRequestDbRepo.save(fr);
    }

    public void approveFriendRequest(User from,User to)
    {
        for(FriendRequest fr: friendRequestDbRepo.findAll())
            if(fr.getFrom().getId()==from.getId() && fr.getTo().getId()==to.getId())
            {
                FriendRequest oldFR=fr;
                fr.setStatus(FriendRequestStatus.APPROVED);
                FriendRequest newFR=fr;
                friendRequestDbRepo.update(oldFR,newFR);

            }
    }
    public void rejectFriendRequest(User from,User to)
    {
        for(FriendRequest fr: friendRequestDbRepo.findAll())
            if(fr.getFrom().getId()==from.getId() && fr.getTo().getId()==to.getId())
            {
                FriendRequest oldFR=fr;
                fr.setStatus(FriendRequestStatus.REJECTED);
                FriendRequest newFR=fr;
                friendRequestDbRepo.update(oldFR,newFR);
            }
    }

}
