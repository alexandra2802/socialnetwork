package ro.ubbcluj.map.service;

import ro.ubbcluj.map.model.Friendship;
import ro.ubbcluj.map.model.User;

import java.util.*;

public class Network {
    private List<Friendship> edges;
    private List<User> vertices;
    private int v; //number of vertices
    private int[] visited;
    private UserService userService;
    private FriendshipService friendshipService;

    public Network(UserService userService, FriendshipService friendshipService) {
        this.userService = userService;
        this.friendshipService = friendshipService;

        v = userService.getSize();
        vertices = new ArrayList<User>();
        edges = new ArrayList<Friendship>();
        for (User u : userService.findAll())
            vertices.add(u);
        for (Friendship f : friendshipService.findAll())
            edges.add(f);
        visited = new int[v + 1];
        Arrays.fill(this.visited, 0);


    }

    HashMap<Integer, User> mapUsers()
    {
        HashMap<Integer, User> users = new HashMap<>();
        int contor = 1;
        for (User u : vertices) {
            users.put(contor, u);
            contor++;
        }
        return users;
    }

    public int getUserKey(User user) {
        HashMap<Integer, User> users = mapUsers();
        for (Map.Entry<Integer, User> entry : users.entrySet())
            if (entry.getValue().equals(user)) {
                return entry.getKey();
            }
        return -1;
    }


    public HashSet<Long> DFS(long startUserId)
    {
        HashSet<Integer> result=new HashSet<>(); //parcurgerea cu numar de ordine
        HashSet<Long> finalResult=new HashSet<>(); //parcurgerea cu id-uri
        //visited=new int[v+1];
        //Arrays.fill(this.visited,0);
        Stack<Integer> stack=new Stack<>();
        User startUser=userService.findOne(startUserId);
        int startUserKey=getUserKey(startUser);
        stack.push(startUserKey);
        visited[startUserKey]=1;
        while (!stack.isEmpty())
        {
            int x=stack.pop();
            result.add(x);
            for (Friendship e:edges)
            {
                User user1=userService.findOne(e.getuser1ID());
                User user2=userService.findOne(e.getuser2ID());
                if(getUserKey(user1)==x && visited[getUserKey(user2)]!=1)
                {
                    stack.push(getUserKey(user2));
                    visited[getUserKey(user2)]=1;
                }
                else if(getUserKey(user2)==x && visited[getUserKey(user1)]!=1)
                {
                    stack.push(getUserKey(user1));
                    visited[getUserKey(user1)]=1;
                }
            }
        }
        for(int i:result)
            finalResult.add(mapUsers().get(i).getId());
        return finalResult;
    }

    public ArrayList<HashSet<Long>> communities()
    //returns a list of communities(connected components)
    {
        ArrayList<HashSet<Long>> communities=new ArrayList<HashSet<Long>>();
        int connectedComp=0;
        for(User u:vertices)
        {
            if(visited[getUserKey(u)]==0)
            {
                communities.add(DFS(u.getId()));
            }
        }
        Arrays.fill(visited,0);
        return communities;
    }

    public int numberOfCommunities()
    {
        ArrayList<HashSet<Long>> comm=this.communities();
        return comm.size();
    }


    public HashSet<Long> mostSociableCommunity()
    //returns community(list of user IDs) with max number of edges
    {
        int maxNrEdges=0;
        HashSet<Long> result=new HashSet<>();
        for(HashSet<Long> community: communities())
        {
            int nrEdges=0;
            for(Friendship e:edges)
            {
                if(community.contains(e.getuser1ID())&& community.contains(e.getuser2ID()))
                    nrEdges++;
            }
            if(nrEdges>maxNrEdges)
            {
                maxNrEdges=nrEdges;
                result=community;
            }
        }
        return result;
    }

    }

