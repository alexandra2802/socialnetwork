package ro.ubbcluj.map.service;

import ro.ubbcluj.map.model.FriendRequest;
import ro.ubbcluj.map.model.FriendRequestStatus;
import ro.ubbcluj.map.model.User;
import ro.ubbcluj.map.repository.db.FriendRequestDbRepository;

public class FriendRequestService {
    FriendRequestDbRepository friendRequestDbRepository;

    public void add(User from, User to, FriendRequestStatus status)
    {
        FriendRequest fr=new FriendRequest(from,to ,status);
        friendRequestDbRepository.save(fr);
    }
}
