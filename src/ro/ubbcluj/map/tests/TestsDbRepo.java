//package ro.ubbcluj.map.tests;
//
//import ro.ubbcluj.map.model.Friendship;
//import ro.ubbcluj.map.model.User;
//import ro.ubbcluj.map.model.validators.FriendshipValidator;
//import ro.ubbcluj.map.model.validators.UserValidator;
//import ro.ubbcluj.map.repository.Repository;
//import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
//import ro.ubbcluj.map.repository.db.UserDbRepository;
//
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.Objects;
//import java.util.Set;
//
//public class TestsDbRepo {
//
//    public void test()
//    {
//        Repository<Long, User> UserDbRepo=new UserDbRepository
//                ("jdbc:postgresql://localhost:5432/SocialNetwork",
//                "postgres",
//                "Alxndr28022001",
//                new UserValidator());
//        //assert repoDb.getSize()==6;
//        assert Objects.equals(UserDbRepo.findOne(1L).getFirst_name(), "Ion");
//        assert Objects.equals(UserDbRepo.findOne(1L).getLast_name(), "Pop");
//        assert UserDbRepo.findOne(90L)==null;
//        for(User u:UserDbRepo.findAll())
//        {
//            assert u.equals(UserDbRepo.findOne(u.getId()));
//        }
//        User u=new User("Raluca","Florea");
//        UserDbRepo.update(UserDbRepo.findOne(2L),u);
//        assert Objects.equals(UserDbRepo.findOne(2L).getFirst_name(), "Raluca");
//        assert Objects.equals(UserDbRepo.findOne(2L).getLast_name(), "Florea");
//
//
//        //repoDb.remove(repoDb.findOne(6L));
//        //assert repoDb.getSize()==5;
////        User newUser=new User("Marcel","Titu");
////        repoDb.save(newUser);
////        assert repoDb.getSize()==6;
//
//        Repository<Long, Friendship> friendshipDbRepo=new FriendshipDbRepository
//                        ("jdbc:postgresql://localhost:5432/SocialNetwork",
//                        "postgres",
//                        "Alxndr28022001",
//                        new FriendshipValidator());
//        assert friendshipDbRepo.findOne(1L).getuser1ID()==1L;
//        assert friendshipDbRepo.findOne(1L).getuser2ID()==2L;
////        assert friendshipDbRepo.findOne(2L).getuser1ID()==2L;
////        assert friendshipDbRepo.findOne(2L).getuser2ID()==3L;
//       // assert friendshipDbRepo.getSize()==2;
//
//        Friendship newFriendship=new Friendship(1L,3L);
//        friendshipDbRepo.update(friendshipDbRepo.findOne(2L),newFriendship);
//        assert friendshipDbRepo.findOne(2L).getuser1ID()==1L;
//        assert friendshipDbRepo.findOne(2L).getuser2ID()==3L;
//
//
//    }
//}
