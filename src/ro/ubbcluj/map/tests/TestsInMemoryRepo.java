//package ro.ubbcluj.map.tests;
//
//import ro.ubbcluj.map.model.Friendship;
//import ro.ubbcluj.map.model.User;
//import ro.ubbcluj.map.model.validators.RepoException;
//import ro.ubbcluj.map.model.validators.UserValidator;
//import ro.ubbcluj.map.model.validators.Validator;
//import ro.ubbcluj.map.repository.InMemoryRepository;
//
//public class TestsInMemoryRepo {
//    public void test() {
//        User u1 = new User("Ion", "Pop");
//        u1.setId(1L);
//        User u2 = new User("Maria", "Popescu");
//        u2.setId(2L);
//
//        Validator<User> validator = new UserValidator();
//        InMemoryRepository<Long, User> repo = new InMemoryRepository<Long, User>(validator);
//        repo.save(u1);
//        assert (repo.findOne(1L) != null);
//        try
//        {
//           repo.findOne(2L);
//        }
//        catch (RepoException e)
//        {
//            assert(true);
//        }
//        assert (repo.getSize() == 1);
//
//        repo.remove(u1);
//        try
//        {
//            repo.findOne(1L) ;
//
//        }catch (RepoException e)
//        {
//            assert true;
//        }
//        assert (repo.getSize() == 0);
//
//        repo.save(u1);
//        repo.save(u2);
//        repo.remove(u2);
//        assert (repo.findOne(1L) != null);
//        try
//        {
//            repo.findOne(2L);
//        }catch (RepoException e)
//        {
//            assert true;
//        }
//
//
//        repo.update(u1,u2);
//        try
//        {
//            repo.findOne(1L);
//
//        }catch (RepoException e)
//        {
//            assert true;
//        }
//        assert (repo.findOne(2L) != null);
//
//        u1.addFriend(u2);
//        assert u1.getFriends_list().contains(u2);
//        assert u2.getFriends_list().contains(u1);
//
//        Friendship f=new Friendship(u1.getId(),u2.getId(),"01.01.2000");
//
//    }
//
//}
