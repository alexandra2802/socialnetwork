//package ro.ubbcluj.map.tests;
//
//import ro.ubbcluj.map.model.Friendship;
//import ro.ubbcluj.map.model.User;
//import ro.ubbcluj.map.model.validators.FriendshipValidator;
//import ro.ubbcluj.map.model.validators.RepoException;
//import ro.ubbcluj.map.model.validators.UserValidator;
//import ro.ubbcluj.map.model.validators.Validator;
//import ro.ubbcluj.map.repository.FriendshipFileRepository;
//import ro.ubbcluj.map.repository.UserFileRepository;
//
//import java.io.IOException;
//
//public class TestsFileRepo {
//    public void test()
//    {
//        Validator<User> validator = new UserValidator();
//        UserFileRepository userFileRepo=new UserFileRepository(validator,"C:\\Users\\Alexandra\\Desktop\\MAP\\SocialNetwork\\src\\ro\\ubbcluj\\map\\tests\\test.in");
//        assert(userFileRepo.getSize()==2);
//        userFileRepo.setFilename("C:\\Users\\Alexandra\\Desktop\\MAP\\SocialNetwork\\src\\ro\\ubbcluj\\map\\tests\\test.out");
//        User u=new User("Laura","Enache");
//        u.setId(3L);
//        try {
//            userFileRepo.addEntity(u);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        assert userFileRepo.getSize()==3;
//
//        try {
//            userFileRepo.deleteEntity(u);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        assert userFileRepo.getSize()==2;
//        try
//        {
//            userFileRepo.findOne(3L);
//        }catch (RepoException e)
//        {
//            assert true;
//        }
//
//        FriendshipValidator validator2=new FriendshipValidator();
//        FriendshipFileRepository friendshipFileRepository=new FriendshipFileRepository("C:\\Users\\Alexandra\\Desktop\\MAP\\SocialNetwork\\data\\testFriendship.in",
//                validator2);
//        assert friendshipFileRepository.getSize()==2;
//
//        friendshipFileRepository.setFilename("C:\\Users\\Alexandra\\Desktop\\MAP\\SocialNetwork\\data\\testFriendship.out");
//        User u2=new User("Ana","Pop");
//        u2.setId(4L);
//        Friendship f=new Friendship(u.getId(),u2.getId(),"");
//        f.setId(31L);
//        try {
//            friendshipFileRepository.addEntity(f);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        assert friendshipFileRepository.getSize()==3;
//        try {
//            friendshipFileRepository.deleteEntity(f);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        assert friendshipFileRepository.getSize()==2;
//    }
//
//}
