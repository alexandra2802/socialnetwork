//package ro.ubbcluj.map.tests;
//
//import ro.ubbcluj.map.model.Friendship;
//import ro.ubbcluj.map.model.User;
//import ro.ubbcluj.map.model.validators.FriendshipValidator;
//import ro.ubbcluj.map.model.validators.UserValidator;
//import ro.ubbcluj.map.repository.FriendshipFileRepository;
//import ro.ubbcluj.map.repository.UserFileRepository;
//import ro.ubbcluj.map.service.FriendshipService;
//import ro.ubbcluj.map.service.Network;
//import ro.ubbcluj.map.service.UserService;
//
//import java.util.ArrayList;
//import java.util.HashSet;
//
//public class TestsNetwork {
//    public void test()
//    {
//        UserValidator uv=new UserValidator();
//        UserFileRepository urepo=new UserFileRepository(uv,"C:\\Users\\Alexandra\\Desktop\\MAP\\SocialNetwork\\data\\users.in");
//        UserService us=new UserService(urepo);
//
//        FriendshipValidator fv=new FriendshipValidator();
//        FriendshipFileRepository frepo=new FriendshipFileRepository("C:\\Users\\Alexandra\\Desktop\\MAP\\SocialNetwork\\data\\friendships.in",fv);
//        FriendshipService fs=new FriendshipService(frepo,urepo);
//
//        Network n=new Network(us,fs);
//        HashSet<Long> DFSresult=new HashSet<>();
//        DFSresult.add(2L);
//        DFSresult.add(4L);
//        DFSresult.add(5L);
//        DFSresult.add(3L);
//        DFSresult.add(1L);
//        assert n.DFS(2L).equals(DFSresult);
//
//        assert (n.numberOfCommunities()==2);
//        HashSet<Long> mostSociableComm=new HashSet<>();
//        mostSociableComm.add(1L);
//        mostSociableComm.add(2L);
//        mostSociableComm.add(4L);
//        mostSociableComm.add(5L);
//        mostSociableComm.add(3L);
//        assert n.mostSociableCommunity().equals(mostSociableComm);
//    }
//}
